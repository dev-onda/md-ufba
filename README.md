# Meninas Digitais - UFBA (Noosfero)

> Tema elaborado para a comunidade noosfero do Meninas Digitais UFBA no noosfero

[https://noosfero.ufba.br/profile/meninas-digitais-ufba](https://noosfero.ufba.br/profile/meninas-digitais-ufba)

* Entrega final: 18/11/2019
* Encontros: quartas-feiras às 14h50

#### Milestone 1:

- [x] Criação de layout
- [x] Criação de Style Guide
- [x] Criação de repositório


#### Milestone 2 (16/10 a 24/10):

- [ ] Personalizar banner de apresentação
- [ ] Criação e personalização de menu


#### Milestone 3 (24/10 a 31/10):

- [ ] Personalizar single de notícias
- [ ] Personalização exibição de article
- [ ] Implementação de footer


#### Milestone 4 (31/10 a 07/11):

- [ ] Personalização de bloco de Seguidores da comunidades




